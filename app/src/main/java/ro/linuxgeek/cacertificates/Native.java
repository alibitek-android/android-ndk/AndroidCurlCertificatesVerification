package ro.linuxgeek.cacertificates;

public class Native {
    static {
        System.loadLibrary("native-lib");
    }

    private static final String TAG = Native.class.getSimpleName();

    public native static String stringFromJNI();
    public native static void setCertificateBundleFilePath(String path);
    public native static byte[] downloadUrl(String url);
}
