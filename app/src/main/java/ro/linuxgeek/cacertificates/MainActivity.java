package ro.linuxgeek.cacertificates;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String CERTIFICATE_BUNDLE_FILENAME = "certificate_bundle";

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(Native.stringFromJNI());

        writeCertificateBundle();
        //readCertificateBundle()
        File certificateBundle = new File(getFilesDir().getAbsolutePath().concat(File.separator + CERTIFICATE_BUNDLE_FILENAME));
        String path = certificateBundle.getAbsolutePath();
        Native.setCertificateBundleFilePath(path);
        Log.d(TAG, String.format("Internal path: %s, Size: %d", path, certificateBundle.length()));

        try {
            URL[] urls = {
                new URL("https://wtfismyip.com/json"),
                new URL("https://go-proverbs.github.io"),
            };

            download(urls);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private String readCertificateBundle() {
        BufferedReader br = null;

        try {
            FileInputStream fin = openFileInput(CERTIFICATE_BUNDLE_FILENAME);
            br = new BufferedReader(new InputStreamReader(fin, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while(( line = br.readLine()) != null ) {
                sb.append(line);
                sb.append('\n');
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return "";
    }

    private boolean traverse(File dir) {
        boolean result = false;

        if (dir.exists()) {
            File[] files = dir.listFiles();
            long sz =  0;
            for (File file : files) {
                if (file.isDirectory()) {
                    return traverse(file);
                } else {
                    BufferedReader brin = null;
                    BufferedWriter brout = null;
                    try {
                        long fsz = file.length();
                        Log.d(TAG, String.format("Reading certificate file: %s of size: %d", file.getAbsolutePath(), fsz));
                        sz += fsz;
                        FileInputStream fin = new FileInputStream(file);
                        FileOutputStream fout = openFileOutput(CERTIFICATE_BUNDLE_FILENAME, Context.MODE_APPEND);
                        brin = new BufferedReader(new InputStreamReader(fin, "UTF-8"));
                        brout = new BufferedWriter(new OutputStreamWriter(fout, "UTF-8"));
                        String line;
                        while ((line = brin.readLine()) != null) {
                            brout.write(line + "\n");
                        }
                        result = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (brin != null)
                                brin.close();

                            if (brout != null)
                                brout.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            result = false;
                        }
                    }
                }
            }
            Log.d(TAG, "sz: " + String.valueOf(sz));
        }

        return result;
    }

    private boolean writeCertificateBundle() {
        String ANDROID_ROOT = System.getenv("ANDROID_ROOT");
        String ANDROID_DATA = System.getenv("ANDROID_DATA");
        File CA_CERTS_DIR_SYSTEM = new File(ANDROID_ROOT + "/etc/security/cacerts");
        File CA_CERTS_DIR_ADDED = new File(ANDROID_DATA + "/misc/keychain/cacerts-added");
        File CA_CERTS_DIR_DELETED = new File(ANDROID_DATA + "/misc/keychain/cacerts-removed");

        if (!CA_CERTS_DIR_SYSTEM.exists() || !CA_CERTS_DIR_SYSTEM.isDirectory())
            return false;

        File certificateBundle = new File(getFilesDir().getAbsolutePath().concat(File.separator + CERTIFICATE_BUNDLE_FILENAME));
        if (certificateBundle.isFile() && certificateBundle.exists())
            if (!certificateBundle.delete())
                return false;

        return traverse(CA_CERTS_DIR_SYSTEM);
    }

    private class DownloadTask extends AsyncTask<URL, Integer, Long> {
        private String data;

        protected Long doInBackground(URL... urls) {
            int count = urls.length;
            long totalSize = 0;

            for (int i = 0; i < count; i++)
            {
                byte[] content = Native.downloadUrl(urls[i].toString());
                data = (content == null) ? null : new String(content);
                Log.d(TAG, (data != null) ? ("Downloaded data: " + data) : "Null data");

                if (content != null) {
                    totalSize += content.length;
                }

                publishProgress((int) ((i / (float) count) * 100));

                // Escape early if cancel() is called
                if (isCancelled())
                    break;
            }

            return totalSize;
        }

        protected void onProgressUpdate(Integer... progress) {
            //setProgressPercent(progress[0]);
        }

        protected void onPostExecute(Long result) {
            if (data != null)
                tv.setText(tv.getText() + " " + data);
        }
    }


    private void download(final URL... urls)
    {
        new DownloadTask().execute(urls);
    }
}
