#include <jni.h>
#include <string>
#include <fstream>
#include <cinttypes>
#include <android/log.h>
#include "Curl.h"

static const char *const TAG = "native-lib";
static std::string certificateBundleFilePath;

extern "C"
jstring
Java_ro_linuxgeek_cacertificates_Native_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
void
Java_ro_linuxgeek_cacertificates_Native_setCertificateBundleFilePath(
        JNIEnv* env,
        jobject thiz,
        jstring jcertificateBundleFilePath) {

    const char* certificateBundlePath = NULL;

    if (jcertificateBundleFilePath)
    {
        certificateBundlePath = env->GetStringUTFChars(jcertificateBundleFilePath, NULL);
        const jsize certificateBundlePathLength = env->GetStringUTFLength(jcertificateBundleFilePath);
        certificateBundleFilePath.assign(certificateBundlePath,
                                         (unsigned long) certificateBundlePathLength);

        std::ifstream infile(certificateBundleFilePath, std::ifstream::ate | std::ifstream::binary);

        if (infile.good())
        {
            __android_log_print(ANDROID_LOG_DEBUG, TAG, "Certificate bundle file: %s, size: %" PRIu64, certificateBundlePath, static_cast<unsigned long long>(infile.tellg()));
        }
        else
        {
            __android_log_print(ANDROID_LOG_DEBUG, TAG, "Can't open certificate bundle file: %s", certificateBundlePath);
        }

        infile.close();
    }

    if (certificateBundlePath)
        env->ReleaseStringUTFChars(jcertificateBundleFilePath, certificateBundlePath);
}


extern "C"
jbyteArray
Java_ro_linuxgeek_cacertificates_Native_downloadUrl(JNIEnv *env,
                                                          jobject thiz,
                                                          jstring jurl) {
    const char *url = env->GetStringUTFChars(jurl, 0);
    jbyteArray result = 0;

    if (!url)
        return nullptr;

    Curl curl(certificateBundleFilePath);
    Curl::RequestStatus requestStatus;
    if ( (requestStatus = curl.MakeRequest(url)) == Curl::RequestStatus::OK)
    {
        __android_log_write(ANDROID_LOG_INFO, "cURL", "cURL returned OK!");
        Curl::ResponseType response = curl.GetResponse();
        result = env->NewByteArray((jsize) response->data.size());
        env->SetByteArrayRegion(result, 0, (jsize) response->data.size(), (jbyte*)&response->data[0]);
    }
    else
    {
        __android_log_print(ANDROID_LOG_INFO, "cURL", "cURL returned NOT OK!: %d", static_cast<int>(requestStatus));
    }

    env->ReleaseStringUTFChars(jurl, url);

    return result;
}