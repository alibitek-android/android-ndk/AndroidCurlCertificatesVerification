#include "Curl.h"
#include <algorithm>
#include <android/log.h>

size_t Curl::writerFunction(void *ptr, size_t size, size_t nmemb, void *stream) {
    Curl* self = static_cast<Curl*>(stream);
    if (!self)
        return 0;

    if (self->m_responseHeaders.length())
    {
        __android_log_print(ANDROID_LOG_DEBUG, "cURL", "Response headers: %s", self->m_responseHeaders.data());
    }

    char* data = static_cast<char*>(ptr);
    self->m_response->data.insert(std::end(self->m_response->data), data, data + size * nmemb);

    return size*nmemb;
}

size_t Curl::headerFunction(void *ptr, size_t size, size_t nmemb, void *stream) {
    Curl* self = static_cast<Curl*>(stream);
    if (!self)
        return 0;

    self->m_responseHeaders.append((const char*)ptr, size * nmemb);

    return size * nmemb;
}

int Curl::progressFunction(void *ptr, double dltotal, double dlnow, double ultotal, double ulnow) {
    Curl* self = static_cast<Curl*>(ptr);
    if (!self)
        return 0;

    __android_log_print(ANDROID_LOG_DEBUG, "cURL", "dltotal: %f, dlnow: %f, ultotal: %f, ulnow: %f", dltotal, dlnow, ultotal, ulnow);

    if (dlnow)
    {
        self->m_downloaded = dlnow;
    }

    if (ulnow)
    {
        self->m_uploaded = ulnow;
    }

    return 0;
}

Curl::Curl(const std::string& certificateBundlePath)
        : m_certificateBundlePath(certificateBundlePath),
          m_curlSession(curl_easy_init()),
          m_downloaded(0.0),
          m_uploaded(0.0)
{

}

Curl::~Curl() {
    if (m_curlSession)
    {
        curl_easy_cleanup(m_curlSession);
    }
}

Curl::RequestStatus Curl::MakeRequest(const char* url) {
    if (!m_curlSession)
        return RequestStatus::NoSession;

    __android_log_print(ANDROID_LOG_INFO, "cURL", "Downloading URL: %s", url);

    m_response = std::make_shared<Response>();

    m_downloaded = 0;
    m_uploaded = 0;

    m_error = RequestStatus::OK;
    curl_version_info_data * vinfo = curl_version_info( CURLVERSION_NOW );
    if( vinfo->features & CURL_VERSION_SSL )
    {
        __android_log_write(ANDROID_LOG_INFO, "cURL", "SSL support enabled");
        __android_log_print(ANDROID_LOG_INFO, "cURL", "OpenSSL version:%s", vinfo->ssl_version);
    }
    else
    {
        __android_log_write(ANDROID_LOG_WARN, "cURL", "SSL support NOT enabled in libcURL!");
        return RequestStatus::NoSSL;
    }

    char errorBuffer[CURL_ERROR_SIZE];
    memset(errorBuffer, 0, CURL_ERROR_SIZE);
    curl_easy_setopt(m_curlSession, CURLOPT_ERRORBUFFER, errorBuffer);

    curl_easy_setopt(m_curlSession, CURLOPT_URL, url);

    curl_easy_setopt(m_curlSession, CURLOPT_CONNECTTIMEOUT, 10);

    curl_easy_setopt(m_curlSession, CURLOPT_FOLLOWLOCATION, true);

    curl_easy_setopt(m_curlSession, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(m_curlSession, CURLOPT_WRITEFUNCTION, writerFunction);

    curl_easy_setopt(m_curlSession, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(m_curlSession, CURLOPT_PROGRESSFUNCTION, progressFunction);
    curl_easy_setopt(m_curlSession, CURLOPT_PROGRESSDATA, this);

    curl_easy_setopt(m_curlSession, CURLOPT_HEADERFUNCTION, headerFunction);
    curl_easy_setopt(m_curlSession, CURLOPT_WRITEHEADER, this);

    curl_easy_setopt(m_curlSession, CURLOPT_DNS_CACHE_TIMEOUT, 0);

    curl_easy_setopt(m_curlSession, CURLOPT_CAINFO, m_certificateBundlePath.c_str());
    curl_easy_setopt(m_curlSession, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(m_curlSession, CURLOPT_SSL_VERIFYHOST, 2L);

    CURLcode curlStatus = curl_easy_perform(m_curlSession);
    long httpStatusCode = 0;
    curl_easy_getinfo (m_curlSession, CURLINFO_RESPONSE_CODE, &httpStatusCode);

    if (httpStatusCode != 200 && curlStatus != CURLE_OK){
        size_t len = strlen(errorBuffer);
        std::string error;

        if(len)
        {
            error.assign(errorBuffer);
            error.assign(((errorBuffer[len - 1] != '\n') ? "\n" : ""));
        }
        else
        {
            error.assign(curl_easy_strerror(curlStatus));
        }

        __android_log_print(ANDROID_LOG_DEBUG, "cURL", "CURL fail: %d - %lu - %s", curlStatus, httpStatusCode, error.data());
        curl_easy_cleanup(m_curlSession);
        return RequestStatus::Fail;
    }

    double totalTime, uploadSize, downloadSize, downloadSpeed, uploadSpeed;
    curl_easy_getinfo(m_curlSession, CURLINFO_TOTAL_TIME, &totalTime);
    curl_easy_getinfo(m_curlSession, CURLINFO_SIZE_DOWNLOAD, &downloadSize);
    curl_easy_getinfo(m_curlSession, CURLINFO_SPEED_DOWNLOAD, &downloadSpeed);
    curl_easy_getinfo(m_curlSession, CURLINFO_SIZE_UPLOAD, &uploadSize);
    curl_easy_getinfo(m_curlSession, CURLINFO_SPEED_UPLOAD, &uploadSpeed);

    __android_log_print(ANDROID_LOG_DEBUG, "cURL", "[%p][%d/%d][%s] cUrl stats: Time=%.3fsec Down size=%lld Down speed=%.3fkB/s Up size=%lld Up speed=%.3fkB/s",
                        this,
                        curlStatus,
                        static_cast<int>(m_error),
                        url,
                        totalTime,
                        (long long int) (int64_t)downloadSize,
                        downloadSpeed/1000,
                        (long long int) (int64_t)uploadSize,
                        uploadSpeed/1000);

    __android_log_print(ANDROID_LOG_DEBUG, "cURL", "CURL success: %d", curlStatus);
    return RequestStatus::OK;
}

std::shared_ptr<Curl::Response> Curl::GetResponse() const
{
    return m_response;
}
















