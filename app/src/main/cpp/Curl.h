#ifndef CERTIFICATES_CURL_H
#define CERTIFICATES_CURL_H

#include <curl/curl.h>
#include <string>
#include <memory>
#include <vector>

class Curl
{
public:
    struct Response
    {
        std::vector<char> data;
    };

    typedef std::shared_ptr<Response> ResponseType;

    enum class RequestStatus : uint8_t
    {
        NoSession,
        NoSSL,
        OK,
        Fail
    };
    Curl(const std::string& certificateBundlePath);
    ~Curl();

    RequestStatus MakeRequest(const char* url);

    std::shared_ptr<Curl::Response> GetResponse() const;

protected:
    static size_t headerFunction( void *ptr, size_t size, size_t nmemb, void *stream);

    static size_t writerFunction( void *ptr, size_t size, size_t nmemb, void *stream);

    static int progressFunction(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);

private:
    CURL* m_curlSession;
    std::string m_certificateBundlePath;
    RequestStatus m_error;
    std::string m_responseHeaders;
    double m_downloaded;
    double m_uploaded;
    ResponseType m_response;
};

#endif //CERTIFICATES_CURL_H
