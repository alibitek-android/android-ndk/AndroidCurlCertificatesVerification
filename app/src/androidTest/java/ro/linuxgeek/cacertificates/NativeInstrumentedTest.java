package ro.linuxgeek.cacertificates;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class NativeInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("ro.linuxgeek.cacertificates", appContext.getPackageName());
    }


    @Test
    public void string_Success_FromJNI() {
        assertThat(Native.stringFromJNI(), is("Hello from C++"));
    }

    @Test
    public void curl_Success_CertificateBundleIsSet() {
        byte[] bresult = Native.downloadUrl("https://wtfismyip.com/json");
        assertNotNull(bresult);
        String sresult = new String(bresult);
        assertThat(sresult.length(), greaterThan(0));
        assertThat(sresult, containsString("Your"));
    }

    @Test
    public void curl_Failure_WhenNoCertificateBundleIsSet(){
        Native.setCertificateBundleFilePath("");
        assertNull(Native.downloadUrl("https://wtfismyip.com/json"));
    }

    @Test
    public void curl_Success_WhenCertificateBundleIsSet_And_HTTP_Protocol() {
        byte[] bresult = Native.downloadUrl("http://go-proverbs.github.io");
        assertNotNull(bresult);
        String sresult = new String(bresult);
        assertThat(sresult.length(), greaterThan(0));
        assertThat(sresult, containsString("Simple, Poetic, Pithy"));
    }

    @Test
    public void curl_Success_WhenNoCertificateBundleIsNotSet_And_HTTP_Protocol(){
        Native.setCertificateBundleFilePath("");
        byte[] bresult = Native.downloadUrl("http://go-proverbs.github.io");
        assertNotNull(bresult);
        String sresult = new String(bresult);
        assertThat(sresult.length(), greaterThan(0));
        assertThat(sresult, containsString("Simple, Poetic, Pithy"));
    }

    @Test
    public void curl_Failure_WhenInvalidDomain() {
        assertNull(Native.downloadUrl("https://linuxgeek.test"));
    }
}
